@extends('layouts.backend')


@section('title',trans('dashboard.label.dashboard'))

@push('js')
<style>

</style>
@endpush

@section('content')

    <div class="box bordered-box blue-border">
        <div class="box-header blue-background">
            <div class="title">
                <i class="icon-circle-blank"></i>
                @lang('dashboard.label.dashboard')
            </div>

        </div>
        <div class="panel-body">


            <div class="row">

            </div>


            <div class="table-responsive">


            </div>

        </div>
    </div>

@endsection



@push('js')
<script>

</script>


@endpush
