<!-- Core JS files -->
<script type="text/javascript" src="{!! asset('user-backend/assets/js/plugins/loaders/pace.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('user-backend/assets/js/core/libraries/jquery.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('user-backend/assets/js/core/libraries/bootstrap.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('user-backend/assets/js/plugins/loaders/blockui.min.js') !!}"></script>
<!-- /core JS files -->

<!-- Theme JS files -->
<script type="text/javascript" src="{!! asset('user-backend/assets/js/plugins/visualization/d3/d3.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('user-backend/assets/js/plugins/visualization/d3/d3_tooltip.js') !!}"></script>
<script type="text/javascript" src="{!! asset('user-backend/assets/js/plugins/forms/styling/switchery.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('user-backend/assets/js/plugins/forms/styling/uniform.min.js') !!}"></script>
<script type="text/javascript" src="{!! asset('user-backend/assets/js/plugins/forms/selects/bootstrap_multiselect.js') !!}"></script>

<script type="text/javascript" src="{!! asset('user-backend/assets/js/core/app.js') !!}"></script>
<!-- /theme JS files -->
<script src="{!! asset('assets/javascripts/toastr.min.js')!!}"></script>
<script src="{!! asset('assets/javascripts/moment.min.js')!!}"></script>