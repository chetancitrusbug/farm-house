<?php

return [


    'label'=>[
        'select_from_dropdown' => 'sélectionner..',
        'add_new' => 'Ajouter un nouveau',
        'mark' => 'marque',
        'action' => 'actes',
        'cancel' => 'Annuler',
        'save' => 'sauvegarder',
        'update' => 'Mettre à jour',
        'submit' => 'Soumettre',
        'create' => 'Créer',
        'status' => 'Statut',
        'edit' => 'modifier',
        'back' => 'Arrière',
        'delete' => 'Effacer',
        'created' => 'Créé',
        'select_subject' => "Sélectionner un sujet",
        'select_site' => "Sélectionnez le site",
    ],
    'datatable' =>[
        'search' => 'Chercher',
        'show' => 'Montrer',
        'entries' => 'Entrées',
        'showing' => 'Montrer',
        'to' => 'à',
        'of' => 'de',
        'small_entries' => 'entrées',
        'paginate' => [
            'next' => 'Prochain',
            'previous' => 'précédent',
            'first' => 'Premier',
            'last' => 'Dernier',
        ],

    ],
    'daterange' =>[
        'all' => 'Tout',
        'today' => "Aujourd'hui",
        'yesterday' => 'Hier',
        'last7day' => 'Les 7 derniers jours',
        'last30day' => 'Les 30 derniers jours',
        'thismonth' => 'Ce mois-ci',
        'lastmonth' => 'Le mois dernier',
        'thisyear' => 'Cette année',
        'lastyear' => "L'année dernière",
        'customeRange' => 'Gamme Custome',
        'applyBtn' => 'Appliquer',
        'cancelBtn' => 'Annuler',
    ],
    'responce_msg' =>[
        'something_went_wrong' => "Quelque chose c'est mal passé. Merci d'essayer plus tard.\",
        'you_have_no_permision_to_delete_record' => \"Vous n'avez pas la permission de supprimer cet enregistrement",
        'record_deleted_succes' => 'Enregistrement supprimé Succès',
    ],
    'js_msg' =>[
        'confirm_for_delete' => 'Etes-vous sûr de supprimer :item_name?',
    ]

];
