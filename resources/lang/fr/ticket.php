<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'label'=>[
        'mark_ticket_as' => 'Marquer le ticket comme ...',
        'mark_as' => 'Marquer comme',
        'evidence' => 'Preuve',
        'note' => 'Remarque',
        'place_a_comment' => 'Placer un commentaire',
        'id' => 'Id',
        'name' => 'prénom',
        'title' => 'Titre',
        'subject' => 'Assujettir',
        'content' => 'Contenu',
        'status' => 'Statut',
        'site' => 'Site',
        'equipment_id' => "Identifiant de l'équipement",
        'website' => 'Site Internet',
        'create_ticket' => 'Créer un ticket',
        'edit_ticket' => 'Modifier le ticket',
        'added_a_file' => "Ajout d'un fichier",
        'show_ticket' => "Afficher le billet",
        'add_your_comment_placeorder' => 'Tapez votre commentaire ici ....',
    ],
];
