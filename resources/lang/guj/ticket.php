<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'label'=>[
        'mark_ticket_as' => 'ટિકિટ માર્ક કરો ...',
        'mark_as' => 'તરીકે ચિહ્નિત કરો',
        'evidence' => 'પુરાવા',
        'note' => 'નૉૅધ',
        'place_a_comment' => 'ટિપ્પણી મૂકો',
        'id' => 'Id',
        'name' => 'નામ',
        'title' => 'શીર્ષક',
        'subject' => 'વિષય',
        'content' => 'સામગ્રી',
        'status' => 'સ્થિતિ',
        'site'=>'Site',
        'equipment_id' => 'સાધન Id',
        'website' => 'વેબસાઇટ',
        'create_ticket' => 'ટિકિટ બનાવો',
        'edit_ticket' => 'ટિકિટ સંપાદિત કરો',
        'added_a_file' => 'એક ફાઇલ ઉમેરાઈ',
        'show_ticket' => 'ટિકિટ બતાવો',
        'add_your_comment_placeorder' => 'તમારી ટિપ્પણી અહીં લખો ....',
    ],
];
