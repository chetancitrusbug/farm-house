<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
 */
    'add_variety' => 'Add Variety',
    'add_new_variety' => 'Add New Variety',
    'back' => 'Back',
    'edit_variety' => 'Edit Variety',
    'update' => 'Update',
    'create' => 'Create',
    'varietys' => 'Varieties',
    'id' => 'ID',
    'name' => 'Name',
    'status' => 'Status',
    'actions' => 'Actions',
    'blocked' => 'Blocked',
    'active' => 'Active',
    'inactive' => 'Inactive',
    'view' => 'View',
    'logs' => 'Logs',
    'view_variety' => 'View Variety',
    'variety' => 'Variety',
    'last_login_at' => 'Last Login At',   

];
