<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
 */
    'add_supplier' => 'Add Supplier',
    'add_new_supplier' => 'Add New Supplier',
    'back' => 'Back',
    'edit_supplier' => 'Edit Supplier',
    'update' => 'Update',
    'email' => 'Email',
    'country'=>'Country',
    'password' => 'Password',
    'role' => 'Role',
    'comments' => 'Comments',
    'create' => 'Create',
    'suppliers' => 'Suppliers',
    'id' => 'ID',
    'profile' => 'Profile',
    'name' => 'Name',
    'email' => 'Email',
    'role' => 'Role',
    'company' => 'Company',
    'status' => 'Status',
    'actions' => 'Actions',
    'blocked' => 'Blocked',
    'active' => 'Active',
    'inactive' => 'Inactive',
    'view' => 'View',
    'logs' => 'Logs',
    'view_supplier' => 'View Supplier',
    'supplier' => 'Supplier',
    'last_login_at' => 'Last Login At',   

];
