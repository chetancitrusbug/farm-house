<?php

return [


    'label'=>[
    	'e_mail' => 'E-mail',
        'password' => 'Password',
        'remember' => 'Remeber',
        'submit' => 'Submit',
        'sign_up' => 'Sign Up',
        'register' => 'Register',
        'log_in' => 'Log In',
        'remember_me' => 'Remember me',
        'sign_in' => 'Sign In',
        'forgot_your_password' => 'Forgot Your Password',

    ]

];