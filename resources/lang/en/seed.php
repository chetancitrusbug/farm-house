<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
 */
    'add_seed' => 'Add Seed',
    'add_new_seed' => 'Add New Seed',
    'back' => 'Back',
    'edit_seed' => 'Edit Seed',
    'update' => 'Update',
    'role' => 'Role',
    'create' => 'Create',
    'id' => 'ID',
    'profile' => 'Profile',
    'role' => 'Role',
    'company' => 'Company',
    'status' => 'Status',
    'actions' => 'Actions',
    'blocked' => 'Blocked',
    'active' => 'Active',
    'inactive' => 'Inactive',
    'view' => 'View',
    'logs' => 'Logs',
    'seed' => 'Seed',
    'last_login_at' => 'Last Login At', 
    'type'=>'Seed Name',  
    'amount'=>'Seed Density',
    'measurement'=>'Measurement',
    'soak_status'=>'Soak',
    'germination'=>'Germination Days',
    'maturity'=>'Days To Maturity',
    'yield'=>'Yield',
    'seeds_measurement'=>'Measurement',
    'seeds'=>'Seeds',
    'supplier_id'=>'Supplier',
    'variety'=>'Variety',
    'tray_size'=>'Tray Size',
    'situation'=>'Situation',
    'medium'=>'Best Medium',
    'notes'=>'Grow Notes',

];
