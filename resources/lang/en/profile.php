<?php

return [

	'my_profile' => 'My Profile',
	'edit_profile' => 'Update Profile',
	'change_password' => 'Change Password',
	'name' => 'Name',
	'email' => 'E-mail',
	'language' => 'Language',
	'joined' => 'Joined',
	'back' => 'Back',
	'current_password' => 'Current Password',
	'password' => 'Password',
	'password_confirmation' => 'Password Confirmation',
	'update_profile' => 'Update Profile',
	'first_name'=>'First Name',
	'last_name'=>'Last Name',
	

];