<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
 */
    'add_user' => 'Add User',
    'add_new_user' => 'Add New User',
    'back' => 'Back',
    'edit_user' => 'Edit User',
    'update' => 'Update',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'contact_no' => 'Contact No',
    'gender'=>'Gender',
    'male'=>'Male',
    'female'=>'Female',
    'date_of_birth'=>'Date Of Birth',
    'expected_date_of_death'=>'Expected Date Of Death',
    'email' => 'Email',
    'country'=>'Country',
    'password' => 'Password',
    'role' => 'Role',
    'user_image' => 'Profile',
     'update_image' => 'Change Profile',
    'comments' => 'Comments',
    'create' => 'Create',
    'users' => 'Users',
    'id' => 'ID',
    'profile' => 'Profile',
    'name' => 'Name',
    'email' => 'Email',
    'role' => 'Role',
    'company' => 'Company',
    'status' => 'Status',
    'actions' => 'Actions',
    'blocked' => 'Blocked',
    'active' => 'Active',
    'inactive' => 'Inactive',
    'view' => 'View',
    'logs' => 'Logs',
    'view_user' => 'View User',
    'user' => 'User',
    'last_login_at' => 'Last Login',   

];
