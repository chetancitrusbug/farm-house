/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.6.39 : Database - farm_house
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`farm_house` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `farm_house`;

/*Table structure for table `contact` */

DROP TABLE IF EXISTS `contact`;

CREATE TABLE `contact` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `contact` */

insert  into `contact`(`id`,`name`,`email`,`phone`,`subject`,`deleted_at`,`created_at`,`updated_at`) values (2,'hetal','hetal3@gmail.com','4567889823','e',NULL,'2018-05-01 09:35:34','2018-05-01 09:35:34'),(3,'wewqe','rwe@gmail.com','4567889823','test forr mail',NULL,'2018-05-01 09:36:31','2018-05-01 09:36:31'),(4,'wewqe','rwe@gmail.com','4567889823','test forr mail',NULL,'2018-05-01 09:37:49','2018-05-01 09:37:49'),(6,'My Contact','test@gmail.com','0987654321','Bug Bounty',NULL,'2018-05-01 10:31:15','2018-05-01 10:31:15'),(7,'wewqe','rwe@gmail.com','4567889823','dfsdf',NULL,'2018-05-01 11:37:41','2018-05-01 11:37:41'),(8,'wewqe','rwe@gmail.com','4567889823','fsd',NULL,'2018-05-01 11:38:51','2018-05-01 11:38:51'),(9,'wewqe','rwe@gmail.com','4567889823','fsd',NULL,'2018-05-01 11:39:09','2018-05-01 11:39:09'),(10,'Tester Softwre','vraj.tester@gmail.com','99876543214545456','erfdgfd',NULL,'2018-05-01 12:07:18','2018-05-01 12:07:18'),(11,'vraj','test12345@gmail.com','0987654321','test subject',NULL,'2018-05-01 12:17:38','2018-05-01 12:17:38');

/*Table structure for table `days` */

DROP TABLE IF EXISTS `days`;

CREATE TABLE `days` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `days` */

insert  into `days`(`id`,`name`,`created_at`,`updated_at`) values (1,'1 DAYS',NULL,NULL),(2,'2 DAYS',NULL,NULL),(3,'3 DAYS',NULL,NULL),(4,'4 DAYS',NULL,NULL),(5,'5 DAYS',NULL,NULL),(6,'6 DAYS',NULL,NULL),(7,'7 DAYS',NULL,NULL),(8,'8 DAYS',NULL,NULL),(9,'9 DAYS',NULL,NULL),(10,'10 DAYS',NULL,NULL),(11,'11 DAYS',NULL,NULL),(12,'12 DAYS',NULL,NULL),(13,'13 DAYS',NULL,NULL),(14,'14 DAYS',NULL,NULL),(15,'15 DAYS',NULL,NULL),(16,'16 DAYS',NULL,NULL),(17,'17 DAYS',NULL,NULL),(18,'18 DAYS',NULL,NULL),(19,'19 DAYS',NULL,NULL),(20,'20 DAYS',NULL,NULL),(21,'21 DAYS',NULL,NULL),(22,'22 DAYS',NULL,NULL),(23,'23 DAYS',NULL,NULL),(24,'24 DAYS',NULL,NULL),(25,'25 DAYS',NULL,NULL),(26,'26 DAYS',NULL,NULL),(27,'27 DAYS',NULL,NULL),(28,'28 DAYS',NULL,NULL),(29,'29 DAYS',NULL,NULL),(30,'30 DAYS',NULL,NULL);

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_193651_create_roles_permissions_tables',1),(6,'2018_04_24_071227_create_days_table',1),(7,'2018_04_24_101805_create_variety_table',1),(8,'2018_04_24_113645_seed_variety_table',1),(10,'2018_04_24_121356_create_seed_supplier_table',1),(11,'2014_10_12_000000_create_users_table',2),(12,'2018_04_24_053928_create_seeds_table',3),(13,'2018_04_24_054719_seeds_detail_table',3),(14,'2018_04_26_114727_create_userseed_table',3),(15,'2018_04_30_065303_create_userseed_detail_table',4),(16,'2018_04_24_121031_create_supplier_table',5),(17,'2018_05_01_042432_add_field_variety_table',6),(18,'2018_03_23_053821_create_contact_table',7),(19,'2018_03_28_055905_create_settings_table',8);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

insert  into `password_resets`(`email`,`token`,`created_at`) values ('chetan.citrusbug@gmail.com','$2y$10$zxSxVLeyxXkyXU2t.3QaWeefBfGjWjgRsRrBxvzGyZBuHTBhHm6RO','2018-04-25 13:16:17');

/*Table structure for table `permission_role` */

DROP TABLE IF EXISTS `permission_role`;

CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permission_role` */

insert  into `permission_role`(`permission_id`,`role_id`) values (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1);

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`parent_id`,`name`,`label`,`deleted_at`,`created_at`,`updated_at`) values (1,0,'access.users','Can Access Users',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(2,1,'access.user.create','Can Create User',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(3,1,'access.user.edit','Can Edit User',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(4,1,'access.user.delete','Can Delete User',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(5,0,'access.roles','Can Access Roles',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(6,5,'access.role.create','Can Create Role',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(7,5,'access.role.edit','Can Edit Role',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(8,5,'access.role.delete','Can Delete Role',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(9,0,'access.permissions','Can Access Permission',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(10,9,'access.permission.create','Can Create Permission',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(11,9,'access.permission.edit','Can Edit Permission',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(12,9,'access.permission.delete','Can Delete Permission',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(13,0,'access.customers','Can Access Customers',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(14,13,'access.customers.create','Can Create Customers',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(15,13,'access.customers.edit','Can Edit Customers',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(16,13,'access.customers.delete','Can Delete Customers',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(17,0,'access.suppliers','Can Access Suppliers',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(18,17,'access.suppliers.create','Can Create Suppliers',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(19,17,'access.suppliers.edit','Can Edit Suppliers',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(20,17,'access.suppliers.delete','Can Delete Suppliers',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(21,0,'access.banks','Can Access Banks',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(22,21,'access.banks.create','Can Create Banks',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(23,21,'access.banks.edit','Can Edit Banks',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(24,21,'access.banks.delete','Can Delete Banks',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(25,0,'access.invoices','Can Access Invoices',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(26,25,'access.invoices.create','Can Create Invoices',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(27,25,'access.invoices.edit','Can Edit Invoices',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(28,25,'access.invoices.delete','Can Delete Invoices',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17');

/*Table structure for table `role_user` */

DROP TABLE IF EXISTS `role_user`;

CREATE TABLE `role_user` (
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`user_id`),
  KEY `role_user_user_id_foreign` (`user_id`),
  CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `role_user` */

insert  into `role_user`(`role_id`,`user_id`) values (1,1);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`label`,`deleted_at`,`created_at`,`updated_at`) values (1,'SU','Super User',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(2,'SA','Sub Admin',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17'),(3,'USER','User',NULL,'2018-04-25 12:17:17','2018-04-25 12:17:17');

/*Table structure for table `seed_supplier` */

DROP TABLE IF EXISTS `seed_supplier`;

CREATE TABLE `seed_supplier` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `supplier_seed_id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `seed_supplier` */

insert  into `seed_supplier`(`id`,`supplier_seed_id`,`supplier_id`,`deleted_at`,`created_at`,`updated_at`) values (1,1,1,NULL,'2018-04-27 10:10:20','2018-04-27 14:16:10'),(2,2,1,NULL,'2018-04-27 10:15:02','2018-04-27 10:15:02'),(3,3,2,NULL,'2018-04-27 10:15:35','2018-04-27 10:15:35'),(4,4,1,NULL,'2018-04-27 11:32:19','2018-04-27 13:12:00'),(5,5,1,NULL,'2018-04-27 14:13:32','2018-04-27 14:13:32'),(6,6,2,NULL,'2018-04-30 11:52:05','2018-04-30 11:52:05'),(7,7,1,NULL,'2018-04-30 15:21:01','2018-04-30 15:21:01'),(8,8,7,NULL,'2018-05-01 05:38:13','2018-05-01 05:38:13'),(9,9,1,NULL,'2018-05-01 05:39:43','2018-05-01 05:39:43'),(10,10,10,NULL,'2018-05-01 05:40:38','2018-05-01 07:07:55'),(11,11,1,NULL,'2018-05-01 05:41:30','2018-05-01 05:41:30'),(12,12,1,NULL,'2018-05-01 05:44:52','2018-05-01 05:44:52'),(13,13,10,NULL,'2018-05-01 07:10:32','2018-05-01 07:10:32'),(14,14,10,NULL,'2018-05-01 07:14:01','2018-05-01 07:14:01'),(15,15,11,NULL,'2018-05-01 12:14:29','2018-05-01 12:14:29');

/*Table structure for table `seed_variety` */

DROP TABLE IF EXISTS `seed_variety`;

CREATE TABLE `seed_variety` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `seed_id` int(11) NOT NULL,
  `variety_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `seed_variety` */

insert  into `seed_variety`(`id`,`seed_id`,`variety_id`,`deleted_at`,`created_at`,`updated_at`) values (1,1,1,'2018-04-27 14:15:38','2018-04-27 10:10:20','2018-04-27 14:15:38'),(2,2,1,NULL,'2018-04-27 10:15:02','2018-04-27 10:15:02'),(3,3,6,NULL,'2018-04-27 10:15:35','2018-04-27 10:15:35'),(4,4,2,'2018-04-27 11:34:27','2018-04-27 11:32:19','2018-04-27 11:34:27'),(5,4,2,'2018-04-27 12:28:52','2018-04-27 11:34:27','2018-04-27 12:28:52'),(6,4,2,'2018-04-27 13:12:00','2018-04-27 12:28:52','2018-04-27 13:12:00'),(7,4,2,'2018-04-27 14:04:14','2018-04-27 13:12:00','2018-04-27 14:04:14'),(8,4,2,NULL,'2018-04-27 14:04:14','2018-04-27 14:04:14'),(9,5,2,NULL,'2018-04-27 14:13:32','2018-04-27 14:13:32'),(10,1,1,'2018-04-27 14:16:10','2018-04-27 14:15:38','2018-04-27 14:16:10'),(11,1,1,NULL,'2018-04-27 14:16:10','2018-04-27 14:16:10'),(12,6,6,NULL,'2018-04-30 11:52:05','2018-04-30 11:52:05'),(13,6,7,NULL,'2018-04-30 11:52:05','2018-04-30 11:52:05'),(14,6,8,NULL,'2018-04-30 11:52:05','2018-04-30 11:52:05'),(15,7,1,NULL,'2018-04-30 15:21:01','2018-04-30 15:21:01'),(16,8,1,NULL,'2018-05-01 05:38:13','2018-05-01 05:38:13'),(17,8,2,NULL,'2018-05-01 05:38:13','2018-05-01 05:38:13'),(18,8,3,NULL,'2018-05-01 05:38:13','2018-05-01 05:38:13'),(19,9,1,NULL,'2018-05-01 05:39:43','2018-05-01 05:39:43'),(20,9,8,NULL,'2018-05-01 05:39:43','2018-05-01 05:39:43'),(21,10,13,'2018-05-01 07:07:55','2018-05-01 05:40:38','2018-05-01 07:07:55'),(22,11,18,'2018-05-01 05:48:43','2018-05-01 05:41:30','2018-05-01 05:48:43'),(23,11,19,'2018-05-01 05:48:43','2018-05-01 05:41:30','2018-05-01 05:48:43'),(24,12,1,NULL,'2018-05-01 05:44:52','2018-05-01 05:44:52'),(25,12,25,NULL,'2018-05-01 05:44:52','2018-05-01 05:44:52'),(26,11,18,NULL,'2018-05-01 05:48:43','2018-05-01 05:48:43'),(27,11,19,NULL,'2018-05-01 05:48:43','2018-05-01 05:48:43'),(28,10,13,NULL,'2018-05-01 07:07:55','2018-05-01 07:07:55'),(29,13,175,NULL,'2018-05-01 07:10:32','2018-05-01 07:10:32'),(30,14,14,NULL,'2018-05-01 07:14:01','2018-05-01 07:14:01'),(31,15,181,NULL,'2018-05-01 12:14:29','2018-05-01 12:14:29');

/*Table structure for table `seeds` */

DROP TABLE IF EXISTS `seeds`;

CREATE TABLE `seeds` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `density` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tray_size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `measurement` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'active',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `seeds` */

insert  into `seeds`(`id`,`name`,`density`,`tray_size`,`measurement`,`status`,`deleted_at`,`created_at`,`updated_at`) values (1,'ARUGULA','.78','5 X 5','OUNCES','active','2018-05-01 05:12:42','2018-04-27 10:10:20','2018-05-01 05:12:42'),(2,'ARUGULA 1','.21','10 X 20','OUNCES','active','2018-04-30 11:53:06','2018-04-27 10:15:02','2018-04-30 11:53:06'),(3,'test','.8','10 X 20','GRAMS','active','2018-04-27 11:32:38','2018-04-27 10:15:35','2018-04-27 11:32:38'),(4,'My Seeds','.70','10 X 20','OUNCES','active','2018-05-01 05:45:34','2018-04-27 11:32:19','2018-05-01 05:45:34'),(5,'seeding name','test','10 X 20','OUNCES','active','2018-05-01 05:03:37','2018-04-27 14:13:32','2018-05-01 05:03:37'),(6,'My Personal seed','My Density','10 X 20','OUNCES','inactive','2018-05-01 05:01:34','2018-04-30 11:52:05','2018-05-01 05:01:34'),(7,'ARUGULA TEST','.70','10 X 20','OUNCES','active','2018-05-01 05:15:26','2018-04-30 15:21:01','2018-05-01 05:15:26'),(8,'My Own Seed','Test Seed Density','10 X 20','OUNCES','active','2018-05-01 06:50:33','2018-05-01 05:38:13','2018-05-01 06:50:33'),(9,'Chetan Seed','Chetan Density','5 X 5','OUNCES','active','2018-05-01 07:09:15','2018-05-01 05:39:43','2018-05-01 07:09:15'),(10,'Hasmat Seed','Hasmat density','5 X 5','GRAMS','active','2018-05-07 21:20:03','2018-05-01 05:40:38','2018-05-07 21:20:03'),(11,'Hetal Seed','1200','10 X 20','OUNCES','active','2018-05-01 05:51:36','2018-05-01 05:41:30','2018-05-01 05:51:36'),(12,'A seeds','Test seed density','5 X 5','GRAMS','active','2018-05-01 07:06:24','2018-05-01 05:44:52','2018-05-01 07:06:24'),(13,'Test seeds','Seed Density','10 X 20','GRAMS','active',NULL,'2018-05-01 07:10:32','2018-05-01 07:16:59'),(14,'My personal seeds','as','10 X 20','OUNCES','active','2018-05-07 21:20:15','2018-05-01 07:14:01','2018-05-07 21:20:15'),(15,'Seed Name','Seeds density','10 X 20','GRAMS','active',NULL,'2018-05-01 12:14:29','2018-05-01 12:14:29');

/*Table structure for table `seeds_detail` */

DROP TABLE IF EXISTS `seeds_detail`;

CREATE TABLE `seeds_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `seed_id` int(11) NOT NULL,
  `soak_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `germination` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `situation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medium` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `maturity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yield` int(11) DEFAULT NULL,
  `seeds_measurement` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'active',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `seeds_detail` */

insert  into `seeds_detail`(`id`,`seed_id`,`soak_status`,`germination`,`situation`,`medium`,`maturity`,`yield`,`seeds_measurement`,`notes`,`status`,`deleted_at`,`created_at`,`updated_at`) values (1,1,'1','2','IN DARKNESS','MAT','18',7,'OUNCES','tets','active','2018-05-01 05:10:38','2018-04-27 10:10:20','2018-05-01 05:10:38'),(2,2,'1','16','IN DARKNESS','MAT','18',2,'GRAMS','sds','active','2018-04-30 11:53:06','2018-04-27 10:15:02','2018-04-30 11:53:06'),(3,3,'1','2','IN DARKNESS','SOIL','16',6,'OUNCES','erte','active','2018-04-27 11:32:38','2018-04-27 10:15:35','2018-04-27 11:32:38'),(4,4,'1','2','IN DARKNESS','MAT','9',12,'OUNCES','A convertible note is a form of short-term debt that converts into equity, typically in conjunction with a future financing round; in effect, the investor would be loaning money to a startup','active','2018-05-01 05:45:34','2018-04-27 11:32:19','2018-05-01 05:45:34'),(5,5,'2','15','IN LIGHT','SOIL','15',13,'OUNCES','dfdf','active','2018-05-01 05:03:37','2018-04-27 14:13:32','2018-05-01 05:03:37'),(6,6,'1','11','IN DARKNESS','MAT','1',12,'ML','grow notest','active','2018-05-01 05:01:34','2018-04-30 11:52:05','2018-05-01 05:01:34'),(7,7,'2','2','IN DARKNESS','MAT','17',0,'OUNCES','test','active','2018-05-01 05:15:26','2018-04-30 15:21:01','2018-05-01 05:15:26'),(8,8,'2','1','IN DARKNESS','MAT','18',1200,'GRAMS','My Notes','active',NULL,'2018-05-01 05:38:13','2018-05-01 06:50:33'),(9,9,'2','14','IN DARKNESS','MAT','15',15,'GRAMS','Notes growth','active','2018-05-01 07:09:15','2018-05-01 05:39:43','2018-05-01 07:09:15'),(10,10,'2','14','IN LIGHT','MAT','12',1200,'OUNCES','Test notes','active','2018-05-07 21:20:03','2018-05-01 05:40:38','2018-05-07 21:20:03'),(11,11,'1','6','IN LIGHT','SOIL','14',2000,'OUNCES','Notres','active',NULL,'2018-05-01 05:41:30','2018-05-01 05:51:36'),(12,12,'2','17','IN LIGHT','SOIL','5',1200,'GRAMS','Npotes','active','2018-05-01 07:06:24','2018-05-01 05:44:52','2018-05-01 07:06:24'),(13,13,'1','19','PLANT ON TOP (SOIL)','MAT','16',1000,'OUNCES','test growth notes','active',NULL,'2018-05-01 07:10:32','2018-05-01 07:10:32'),(14,14,'1','20','IN LIGHT','MAT','20',1000,'OUNCES','test notest','active','2018-05-07 21:20:15','2018-05-01 07:14:01','2018-05-07 21:20:15'),(15,15,'1','5','IN LIGHT','MAT','14',1200,'ML','Test Notes of growth of seeds','active',NULL,'2018-05-01 12:14:29','2018-05-01 12:14:29');

/*Table structure for table `setting` */

DROP TABLE IF EXISTS `setting`;

CREATE TABLE `setting` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `youtube` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `googleplus` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactno` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contactno1` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `setting` */

insert  into `setting`(`id`,`email`,`twitter`,`facebook`,`youtube`,`googleplus`,`address`,`contactno`,`contactno1`,`created_at`,`updated_at`) values (1,'vraj.citrusbug@gmail.com','https://twitter.com/login?lang=en','https://www.facebook.com/','','','No 271,Lorem ipsum dolor sit amet consectetur , USA.','+91 (321) 7777567','1800-0000-000-0000',NULL,'2018-05-01 12:48:35');

/*Table structure for table `supplier` */

DROP TABLE IF EXISTS `supplier`;

CREATE TABLE `supplier` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'active',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `supplier` */

insert  into `supplier`(`id`,`name`,`status`,`deleted_at`,`created_at`,`updated_at`) values (1,'TRUELEAF/MOUNTAINVALLEY','active',NULL,'2018-04-30 13:39:40','2018-04-30 13:39:40'),(2,'JOHNNY SEEDS','active',NULL,'2018-04-30 13:39:51','2018-04-30 13:39:51'),(3,'TODD\'S SEEDS','active',NULL,'2018-04-30 13:43:01','2018-04-30 13:43:01'),(5,'KITAZAWA SEED','active',NULL,'2018-04-30 14:25:23','2018-04-30 14:25:23'),(7,'Vraj','active',NULL,'2018-05-01 05:06:43','2018-05-01 05:06:43'),(10,'HASMAT SUPPLIER','active',NULL,'2018-05-01 07:07:25','2018-05-01 07:07:25'),(11,'My Personal supplier','active',NULL,'2018-05-01 12:11:25','2018-05-01 12:11:42');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'active',
  `language` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT 'en',
  `created_by` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL,
  `activation_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activation_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`status`,`language`,`created_by`,`remember_token`,`created_at`,`updated_at`,`is_active`,`activation_token`,`activation_time`) values (1,'wikitu','wikitudedev@gmail.com','$2y$10$/iJSFa8Rt2yaaZ.2mVb3HOJYpOCSmFko32ea/LwF/gn/WanMGEYSm','active','en',0,'aBEmGHAokgAbPucvy35kJPoLHJX44ZL07PXwYmn7CGK5PRgkTckZQAfHlHBb','2018-04-25 12:17:17','2018-04-26 11:22:17',1,NULL,NULL),(39,'hetal','hetal.citrusbug@gmail.com','$2y$10$7kWd.KadFesJ.aI9r/jDyeLpIu9wVPe0vun/dRG37p0pl6dxPqDku','active','en',0,'isXO3Mn4NL8ynMzUm3OkuEIMT38IoB1dHnhpn91vpzF7XljxQmU3fS8uv5Oy','2018-04-27 05:35:43','2018-04-27 05:35:57',1,NULL,NULL),(46,'ssa','sadhana.citrusbug@gmail.com','$2y$10$yxmpEmEBZl0AxvSS3tLxHurZ69AtoYfRL.hZ9Al37W6sEN2q0opsy','active','en',0,'zPbFinRU7F5jTSVljulmBbtDf5WkLdZQPQbmbtuvIR9QUkba8spx7djLWo3J','2018-04-30 13:29:02','2018-04-30 13:29:02',1,'f3bd967c02286fd117fe7f3b763c1e5db862b68d','2018-04-30 13:29:02'),(49,'bhumi patel','kjkcitrusbug@gmail.com','$2y$10$nyRsZf1QbthW4vtnhiIfxOJAkxJvA.ojIn571ojFVDe4yG7tFem0K','active','en',0,NULL,'2018-05-01 10:02:19','2018-05-01 10:02:19',NULL,'29e332b461b3faa0d8a039665eaf02704939958a','2018-05-01 10:02:19'),(51,'Harsh','hasmat.citrusbug@gmail.com','$2y$10$0AeDFoQnLAdMDZ1dLT/edO7hkhWWSk.wdQGSjP6okQh39ogFc1E1e','active','en',0,NULL,'2018-05-01 12:21:43','2018-05-07 21:17:36',1,NULL,NULL),(52,'Ashvini','ashvini.citrusbug@gmail.com','$2y$10$YDyES/VD5ClYM3uczp4PGe/NrVjPBCBJgZbVHq2MHEOARJ4j4kR1m','inactive','en',0,'FCwMQaY9qGXWRXXxQz6RQJrGoCcK4rgsx0oX6QxLj55JpHSP8sJ3ogPR4Rad','2018-05-01 12:25:01','2018-05-01 12:29:19',1,NULL,NULL),(53,'tpsolutions1@verizon.net','tpsolutions1@verizon.net','$2y$10$KztDjA8dNt/EJVPHPOXUq.0ZCgn5RiLiooM1BZOa8ZFSa1ZBPyJrS','active','en',0,'iqCnZJbcEC1yGsCPWIzJvCFL0g5cMnxOpN76PGK85475GrzoSImZbcYHiK1f','2018-05-09 14:47:58','2018-05-11 01:14:17',NULL,'c44c16ff695dc147d197be6f38282c8a00d07a60','2018-05-09 14:47:58'),(54,'Pow','pow@wow.com','$2y$10$wT/mmc2SOodAzao2fi27XeSeCVDdEeAbwyUE9yFb2uWKVbXyedXd.','active','en',0,NULL,'2018-05-14 18:18:19','2018-05-14 18:18:19',NULL,'3c9b29a377009c23b34d84462ecc9b115cbb46ba','2018-05-14 18:18:19');

/*Table structure for table `userseed` */

DROP TABLE IF EXISTS `userseed`;

CREATE TABLE `userseed` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `user_seed_id` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=216 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `userseed` */

insert  into `userseed`(`id`,`user_id`,`user_seed_id`,`status`,`deleted_at`,`created_at`,`updated_at`) values (2,39,1,'active','2018-04-27 13:57:39','2018-04-27 10:16:14','2018-04-27 13:57:39'),(3,39,2,'active','2018-04-27 13:57:39','2018-04-27 10:16:14','2018-04-27 13:57:39'),(6,42,4,'active','2018-04-27 12:29:21','2018-04-27 12:20:00','2018-04-27 12:29:21'),(7,42,4,'active','2018-04-27 12:31:37','2018-04-27 12:29:21','2018-04-27 12:31:37'),(8,42,4,'active','2018-04-27 14:01:20','2018-04-27 12:31:37','2018-04-27 14:01:20'),(9,43,1,'active','2018-04-27 13:05:54','2018-04-27 13:02:23','2018-04-27 13:05:54'),(10,43,1,'active','2018-04-27 14:49:15','2018-04-27 13:05:54','2018-04-27 14:49:15'),(11,39,1,'active','2018-04-30 05:46:56','2018-04-27 13:57:39','2018-04-30 05:46:56'),(12,39,2,'active','2018-04-30 05:46:56','2018-04-27 13:57:39','2018-04-30 05:46:56'),(13,39,4,'active','2018-04-30 05:46:56','2018-04-27 13:57:39','2018-04-30 05:46:56'),(14,42,4,'active','2018-04-27 14:14:17','2018-04-27 14:01:20','2018-04-27 14:14:17'),(15,42,4,'active','2018-04-27 14:14:30','2018-04-27 14:14:17','2018-04-27 14:14:30'),(16,42,4,'active','2018-04-27 14:14:51','2018-04-27 14:14:30','2018-04-27 14:14:51'),(17,42,5,'active','2018-04-27 14:14:51','2018-04-27 14:14:30','2018-04-27 14:14:51'),(18,42,4,'active','2018-04-27 14:16:26','2018-04-27 14:14:51','2018-04-27 14:16:26'),(19,42,5,'active','2018-04-27 14:16:26','2018-04-27 14:14:51','2018-04-27 14:16:26'),(20,42,4,'active','2018-04-27 14:17:05','2018-04-27 14:16:26','2018-04-27 14:17:05'),(21,42,5,'active','2018-04-27 14:17:05','2018-04-27 14:16:26','2018-04-27 14:17:05'),(22,42,4,'active','2018-04-27 14:20:20','2018-04-27 14:17:05','2018-04-27 14:20:20'),(23,42,5,'active','2018-04-27 14:20:20','2018-04-27 14:17:05','2018-04-27 14:20:20'),(24,42,4,'active','2018-04-27 14:20:36','2018-04-27 14:20:20','2018-04-27 14:20:36'),(25,42,5,'active','2018-04-27 14:20:36','2018-04-27 14:20:20','2018-04-27 14:20:36'),(26,42,4,'active','2018-04-27 14:34:38','2018-04-27 14:20:36','2018-04-27 14:34:38'),(27,42,5,'active','2018-04-27 14:34:38','2018-04-27 14:20:36','2018-04-27 14:34:38'),(28,42,4,'active','2018-04-27 14:34:49','2018-04-27 14:34:38','2018-04-27 14:34:49'),(29,42,5,'active','2018-04-27 14:34:49','2018-04-27 14:34:38','2018-04-27 14:34:49'),(30,42,1,'active','2018-04-27 14:34:52','2018-04-27 14:34:49','2018-04-27 14:34:52'),(31,42,2,'active','2018-04-27 14:34:52','2018-04-27 14:34:49','2018-04-27 14:34:52'),(32,42,4,'active','2018-04-27 14:34:52','2018-04-27 14:34:49','2018-04-27 14:34:52'),(33,42,5,'active','2018-04-27 14:34:52','2018-04-27 14:34:49','2018-04-27 14:34:52'),(34,42,1,'active','2018-04-27 14:34:59','2018-04-27 14:34:52','2018-04-27 14:34:59'),(35,42,2,'active','2018-04-27 14:34:59','2018-04-27 14:34:52','2018-04-27 14:34:59'),(36,42,4,'active','2018-04-27 14:34:59','2018-04-27 14:34:52','2018-04-27 14:34:59'),(37,42,5,'active','2018-04-27 14:34:59','2018-04-27 14:34:52','2018-04-27 14:34:59'),(38,42,1,'active','2018-05-01 05:12:27','2018-04-27 14:34:59','2018-05-01 05:12:27'),(39,42,2,'active',NULL,'2018-04-27 14:34:59','2018-04-27 14:34:59'),(40,42,4,'active','2018-05-01 05:45:34','2018-04-27 14:34:59','2018-05-01 05:45:34'),(41,42,5,'active',NULL,'2018-04-27 14:34:59','2018-04-27 14:34:59'),(42,43,1,'active','2018-05-01 05:12:27','2018-04-27 14:49:15','2018-05-01 05:12:27'),(43,43,2,'active',NULL,'2018-04-27 14:49:15','2018-04-27 14:49:15'),(44,39,1,'active','2018-04-30 11:19:25','2018-04-30 05:46:56','2018-04-30 11:19:25'),(45,39,1,'active','2018-04-30 11:46:53','2018-04-30 11:19:25','2018-04-30 11:46:53'),(46,39,4,'active','2018-04-30 11:46:53','2018-04-30 11:19:25','2018-04-30 11:46:53'),(47,39,1,'active','2018-04-30 11:53:43','2018-04-30 11:46:53','2018-04-30 11:53:43'),(48,39,4,'active','2018-04-30 11:53:43','2018-04-30 11:46:53','2018-04-30 11:53:43'),(49,39,1,'active','2018-04-30 12:00:55','2018-04-30 11:53:43','2018-04-30 12:00:55'),(50,39,4,'active','2018-04-30 12:00:55','2018-04-30 11:53:43','2018-04-30 12:00:55'),(51,39,5,'active','2018-04-30 12:00:55','2018-04-30 11:53:43','2018-04-30 12:00:55'),(52,39,6,'active','2018-04-30 12:00:55','2018-04-30 11:53:43','2018-04-30 12:00:55'),(53,39,6,'active','2018-04-30 12:01:37','2018-04-30 12:00:55','2018-04-30 12:01:37'),(54,39,6,'active','2018-04-30 12:10:59','2018-04-30 12:01:37','2018-04-30 12:10:59'),(55,39,1,'active','2018-04-30 12:36:15','2018-04-30 12:10:59','2018-04-30 12:36:15'),(57,40,1,'active','2018-04-30 12:40:58','2018-04-30 12:29:54','2018-04-30 12:40:58'),(58,39,6,'active','2018-04-30 12:36:53','2018-04-30 12:36:15','2018-04-30 12:36:53'),(59,39,6,'active','2018-04-30 12:38:11','2018-04-30 12:36:53','2018-04-30 12:38:11'),(60,39,6,'active','2018-04-30 12:39:48','2018-04-30 12:38:11','2018-04-30 12:39:48'),(61,39,5,'active','2018-05-01 04:47:48','2018-04-30 12:39:48','2018-05-01 04:47:48'),(62,39,6,'active','2018-05-01 04:47:48','2018-04-30 12:39:48','2018-05-01 04:47:48'),(63,40,1,'active','2018-04-30 12:58:52','2018-04-30 12:40:58','2018-04-30 12:58:52'),(64,40,1,'active','2018-05-01 05:12:27','2018-04-30 12:58:52','2018-05-01 05:12:27'),(65,47,1,'active','2018-05-01 00:53:06','2018-04-30 16:13:59','2018-05-01 00:53:06'),(66,47,1,'active','2018-05-01 01:20:12','2018-05-01 00:53:06','2018-05-01 01:20:12'),(67,47,4,'active','2018-05-01 01:20:12','2018-05-01 00:53:06','2018-05-01 01:20:12'),(68,47,5,'active','2018-05-01 01:20:12','2018-05-01 00:53:06','2018-05-01 01:20:12'),(69,47,6,'active','2018-05-01 01:20:12','2018-05-01 00:53:06','2018-05-01 01:20:12'),(70,47,1,'active','2018-05-01 05:12:27','2018-05-01 01:20:12','2018-05-01 05:12:27'),(71,47,4,'active','2018-05-01 05:45:34','2018-05-01 01:20:12','2018-05-01 05:45:34'),(72,47,5,'active','2018-05-02 11:47:00','2018-05-01 01:20:12','2018-05-02 11:47:00'),(73,47,7,'active','2018-05-01 05:15:26','2018-05-01 01:20:12','2018-05-01 05:15:26'),(74,39,5,'active','2018-05-01 04:51:33','2018-05-01 04:47:48','2018-05-01 04:51:33'),(75,39,6,'active','2018-05-01 04:51:33','2018-05-01 04:47:48','2018-05-01 04:51:33'),(76,39,5,'active','2018-05-01 04:58:58','2018-05-01 04:51:33','2018-05-01 04:58:58'),(77,39,5,'active','2018-05-01 04:59:05','2018-05-01 04:58:58','2018-05-01 04:59:05'),(78,39,5,'active','2018-05-01 05:03:11','2018-05-01 04:59:05','2018-05-01 05:03:11'),(79,39,6,'active','2018-05-01 05:03:11','2018-05-01 04:59:05','2018-05-01 05:03:11'),(80,39,7,'active','2018-05-01 05:03:11','2018-05-01 04:59:05','2018-05-01 05:03:11'),(81,39,5,'active','2018-05-01 05:36:15','2018-05-01 05:03:11','2018-05-01 05:36:15'),(82,39,7,'active','2018-05-01 05:15:26','2018-05-01 05:03:11','2018-05-01 05:15:26'),(83,39,4,'active','2018-05-01 05:43:46','2018-05-01 05:36:15','2018-05-01 05:43:46'),(84,39,4,'active','2018-05-01 05:45:34','2018-05-01 05:43:46','2018-05-01 05:45:34'),(85,39,11,'active','2018-05-01 05:46:47','2018-05-01 05:46:00','2018-05-01 05:46:47'),(86,39,11,'active','2018-05-01 05:51:36','2018-05-01 05:46:47','2018-05-01 05:51:36'),(87,48,8,'active','2018-05-01 06:13:19','2018-05-01 06:07:48','2018-05-01 06:13:19'),(88,48,9,'active','2018-05-01 06:13:19','2018-05-01 06:07:48','2018-05-01 06:13:19'),(89,48,9,'active','2018-05-01 06:31:18','2018-05-01 06:13:19','2018-05-01 06:31:18'),(90,39,8,'active','2018-05-01 06:29:25','2018-05-01 06:14:28','2018-05-01 06:29:25'),(91,39,8,'active','2018-05-01 06:34:27','2018-05-01 06:29:25','2018-05-01 06:34:27'),(92,39,9,'active','2018-05-01 06:31:18','2018-05-01 06:29:25','2018-05-01 06:31:18'),(93,39,8,'active','2018-05-01 06:35:00','2018-05-01 06:34:51','2018-05-01 06:35:00'),(94,39,8,'active','2018-05-01 06:35:12','2018-05-01 06:35:00','2018-05-01 06:35:12'),(95,39,8,'active','2018-05-01 06:35:38','2018-05-01 06:35:12','2018-05-01 06:35:38'),(96,39,10,'active','2018-05-01 06:35:38','2018-05-01 06:35:12','2018-05-01 06:35:38'),(97,39,8,'active','2018-05-01 06:36:09','2018-05-01 06:35:38','2018-05-01 06:36:09'),(98,39,10,'active','2018-05-01 06:36:09','2018-05-01 06:35:38','2018-05-01 06:36:09'),(99,39,8,'active','2018-05-01 06:39:26','2018-05-01 06:36:09','2018-05-01 06:39:26'),(100,39,10,'active','2018-05-01 06:39:26','2018-05-01 06:36:09','2018-05-01 06:39:26'),(101,48,8,'active','2018-05-01 06:50:33','2018-05-01 06:37:55','2018-05-01 06:50:33'),(102,39,8,'active','2018-05-01 06:43:17','2018-05-01 06:39:26','2018-05-01 06:43:17'),(103,39,10,'active','2018-05-01 06:43:17','2018-05-01 06:39:26','2018-05-01 06:43:17'),(104,39,12,'active','2018-05-01 06:43:17','2018-05-01 06:39:26','2018-05-01 06:43:17'),(105,39,8,'active','2018-05-01 06:44:26','2018-05-01 06:43:17','2018-05-01 06:44:26'),(106,39,10,'active','2018-05-01 06:44:26','2018-05-01 06:43:17','2018-05-01 06:44:26'),(107,39,12,'active','2018-05-01 06:44:26','2018-05-01 06:43:17','2018-05-01 06:44:26'),(108,39,8,'active','2018-05-01 06:49:18','2018-05-01 06:44:26','2018-05-01 06:49:18'),(109,39,8,'active','2018-05-01 06:50:22','2018-05-01 06:49:18','2018-05-01 06:50:22'),(110,39,10,'active','2018-05-01 06:50:22','2018-05-01 06:49:18','2018-05-01 06:50:22'),(111,39,8,'active','2018-05-01 06:50:33','2018-05-01 06:50:22','2018-05-01 06:50:33'),(112,39,10,'active','2018-05-01 06:51:21','2018-05-01 06:50:22','2018-05-01 06:51:21'),(113,39,12,'active','2018-05-01 06:51:40','2018-05-01 06:50:22','2018-05-01 06:51:40'),(114,39,10,'active','2018-05-01 06:52:26','2018-05-01 06:51:40','2018-05-01 06:52:26'),(115,39,12,'active','2018-05-01 06:52:26','2018-05-01 06:51:40','2018-05-01 06:52:26'),(116,48,10,'active','2018-05-01 06:54:57','2018-05-01 06:51:54','2018-05-01 06:54:57'),(117,39,10,'active','2018-05-01 06:54:57','2018-05-01 06:52:26','2018-05-01 06:54:57'),(118,39,12,'active','2018-05-01 06:55:09','2018-05-01 06:52:26','2018-05-01 06:55:09'),(119,39,10,'active','2018-05-01 07:00:11','2018-05-01 06:55:09','2018-05-01 07:00:11'),(120,39,12,'active','2018-05-01 06:57:41','2018-05-01 06:55:09','2018-05-01 06:57:41'),(121,48,12,'active','2018-05-01 06:57:41','2018-05-01 06:57:18','2018-05-01 06:57:41'),(122,48,10,'active','2018-05-01 07:00:11','2018-05-01 06:58:24','2018-05-01 07:00:11'),(123,48,10,'active','2018-05-01 07:03:51','2018-05-01 07:00:54','2018-05-01 07:03:51'),(124,48,12,'active','2018-05-01 07:03:51','2018-05-01 07:00:54','2018-05-01 07:03:51'),(125,39,9,'active','2018-05-01 07:05:40','2018-05-01 07:03:09','2018-05-01 07:05:40'),(126,39,10,'active','2018-05-01 07:05:40','2018-05-01 07:03:09','2018-05-01 07:05:40'),(127,48,9,'active','2018-05-01 07:03:59','2018-05-01 07:03:51','2018-05-01 07:03:59'),(128,48,10,'active','2018-05-01 07:03:59','2018-05-01 07:03:51','2018-05-01 07:03:59'),(129,48,12,'active','2018-05-01 07:03:59','2018-05-01 07:03:51','2018-05-01 07:03:59'),(130,48,9,'active','2018-05-01 07:04:03','2018-05-01 07:03:59','2018-05-01 07:04:03'),(131,48,10,'active','2018-05-01 07:04:03','2018-05-01 07:03:59','2018-05-01 07:04:03'),(132,48,12,'active','2018-05-01 07:04:03','2018-05-01 07:03:59','2018-05-01 07:04:03'),(133,48,10,'active','2018-05-01 07:04:15','2018-05-01 07:04:03','2018-05-01 07:04:15'),(134,48,12,'active','2018-05-01 07:04:15','2018-05-01 07:04:03','2018-05-01 07:04:15'),(135,48,9,'active','2018-05-01 07:04:28','2018-05-01 07:04:15','2018-05-01 07:04:28'),(136,48,10,'active','2018-05-01 07:04:28','2018-05-01 07:04:15','2018-05-01 07:04:28'),(137,48,12,'active','2018-05-01 07:04:28','2018-05-01 07:04:15','2018-05-01 07:04:28'),(138,48,9,'active','2018-05-01 07:04:32','2018-05-01 07:04:28','2018-05-01 07:04:32'),(139,48,10,'active','2018-05-01 07:04:32','2018-05-01 07:04:28','2018-05-01 07:04:32'),(140,48,12,'active','2018-05-01 07:04:32','2018-05-01 07:04:28','2018-05-01 07:04:32'),(141,48,10,'active','2018-05-01 07:05:09','2018-05-01 07:04:32','2018-05-01 07:05:09'),(142,48,12,'active','2018-05-01 07:05:09','2018-05-01 07:04:32','2018-05-01 07:05:09'),(143,48,9,'active','2018-05-01 07:05:09','2018-05-01 07:05:09','2018-05-01 07:05:09'),(144,48,10,'active','2018-05-01 07:05:09','2018-05-01 07:05:09','2018-05-01 07:05:09'),(145,48,12,'active','2018-05-01 07:05:09','2018-05-01 07:05:09','2018-05-01 07:05:09'),(146,48,9,'active','2018-05-01 07:05:14','2018-05-01 07:05:09','2018-05-01 07:05:14'),(147,48,10,'active','2018-05-01 07:05:14','2018-05-01 07:05:09','2018-05-01 07:05:14'),(148,48,12,'active','2018-05-01 07:05:14','2018-05-01 07:05:09','2018-05-01 07:05:14'),(149,48,10,'active','2018-05-01 07:05:46','2018-05-01 07:05:14','2018-05-01 07:05:46'),(150,48,12,'active','2018-05-01 07:05:46','2018-05-01 07:05:14','2018-05-01 07:05:46'),(151,39,9,'active','2018-05-01 07:06:06','2018-05-01 07:05:40','2018-05-01 07:06:06'),(152,39,10,'active','2018-05-01 07:06:06','2018-05-01 07:05:40','2018-05-01 07:06:06'),(153,48,12,'active','2018-05-01 07:06:24','2018-05-01 07:05:46','2018-05-01 07:06:24'),(154,39,9,'active','2018-05-01 07:08:37','2018-05-01 07:06:06','2018-05-01 07:08:37'),(155,39,10,'active','2018-05-01 07:16:10','2018-05-01 07:06:06','2018-05-01 07:16:10'),(156,39,12,'active','2018-05-01 07:06:24','2018-05-01 07:06:06','2018-05-01 07:06:24'),(157,48,9,'active','2018-05-01 07:08:16','2018-05-01 07:08:06','2018-05-01 07:08:16'),(158,48,10,'active','2018-05-01 07:08:16','2018-05-01 07:08:06','2018-05-01 07:08:16'),(159,48,10,'active','2018-05-01 07:08:28','2018-05-01 07:08:16','2018-05-01 07:08:28'),(160,48,10,'active','2018-05-01 07:08:46','2018-05-01 07:08:28','2018-05-01 07:08:46'),(161,48,10,'active','2018-05-01 07:09:21','2018-05-01 07:08:46','2018-05-01 07:09:21'),(162,48,10,'active','2018-05-01 07:14:15','2018-05-01 07:09:21','2018-05-01 07:14:15'),(163,46,10,'active','2018-05-01 07:16:10','2018-05-01 07:10:50','2018-05-01 07:16:10'),(164,48,10,'active','2018-05-01 07:14:22','2018-05-01 07:14:15','2018-05-01 07:14:22'),(165,48,10,'active','2018-05-01 07:16:10','2018-05-01 07:14:22','2018-05-01 07:16:10'),(166,48,13,'active','2018-05-01 07:16:08','2018-05-01 07:14:22','2018-05-01 07:16:08'),(167,48,14,'active','2018-05-01 07:16:06','2018-05-01 07:14:22','2018-05-01 07:16:06'),(168,48,13,'active','2018-05-01 07:18:06','2018-05-01 07:17:44','2018-05-01 07:18:06'),(169,48,10,'active','2018-05-01 07:19:18','2018-05-01 07:18:06','2018-05-01 07:19:18'),(170,48,13,'active','2018-05-01 07:19:18','2018-05-01 07:18:06','2018-05-01 07:19:18'),(171,48,14,'active','2018-05-01 07:19:18','2018-05-01 07:18:06','2018-05-01 07:19:18'),(172,48,10,'active','2018-05-07 21:20:03','2018-05-01 07:19:18','2018-05-07 21:20:03'),(173,48,13,'active',NULL,'2018-05-01 07:19:18','2018-05-01 07:19:18'),(174,48,14,'active','2018-05-07 21:20:15','2018-05-01 07:19:18','2018-05-07 21:20:15'),(175,50,10,'active','2018-05-07 21:20:03','2018-05-01 10:10:34','2018-05-07 21:20:03'),(176,1,10,'active','2018-05-07 20:02:31','2018-05-01 11:47:00','2018-05-07 20:02:31'),(177,39,10,'active','2018-05-01 12:46:42','2018-05-01 12:15:22','2018-05-01 12:46:42'),(178,39,13,'active','2018-05-01 12:46:42','2018-05-01 12:15:22','2018-05-01 12:46:42'),(179,39,14,'active','2018-05-01 12:46:42','2018-05-01 12:15:22','2018-05-01 12:46:42'),(180,39,10,'active','2018-05-01 12:50:00','2018-05-01 12:46:42','2018-05-01 12:50:00'),(181,39,13,'active','2018-05-01 12:50:00','2018-05-01 12:46:42','2018-05-01 12:50:00'),(182,39,14,'active','2018-05-01 12:50:00','2018-05-01 12:46:42','2018-05-01 12:50:00'),(183,39,10,'active','2018-05-01 12:50:47','2018-05-01 12:50:00','2018-05-01 12:50:47'),(184,39,13,'active','2018-05-01 12:50:47','2018-05-01 12:50:00','2018-05-01 12:50:47'),(185,39,14,'active','2018-05-01 12:50:47','2018-05-01 12:50:00','2018-05-01 12:50:47'),(186,39,10,'active','2018-05-01 12:51:40','2018-05-01 12:50:47','2018-05-01 12:51:40'),(187,39,13,'active','2018-05-01 12:51:40','2018-05-01 12:50:47','2018-05-01 12:51:40'),(188,39,14,'active','2018-05-01 12:51:40','2018-05-01 12:50:47','2018-05-01 12:51:40'),(189,39,10,'active','2018-05-01 12:52:32','2018-05-01 12:51:40','2018-05-01 12:52:32'),(190,39,13,'active','2018-05-01 12:52:32','2018-05-01 12:51:40','2018-05-01 12:52:32'),(191,39,14,'active','2018-05-01 12:52:32','2018-05-01 12:51:40','2018-05-01 12:52:32'),(192,39,10,'active','2018-05-07 21:20:03','2018-05-01 12:52:32','2018-05-07 21:20:03'),(193,39,13,'active',NULL,'2018-05-01 12:52:32','2018-05-01 12:52:32'),(194,39,14,'active','2018-05-07 21:20:15','2018-05-01 12:52:32','2018-05-07 21:20:15'),(195,47,10,'active','2018-05-07 21:20:03','2018-05-02 11:47:00','2018-05-07 21:20:03'),(196,1,10,'active','2018-05-07 20:03:22','2018-05-07 20:02:31','2018-05-07 20:03:22'),(197,1,13,'active','2018-05-07 20:03:22','2018-05-07 20:02:31','2018-05-07 20:03:22'),(198,1,10,'active','2018-05-07 20:04:07','2018-05-07 20:03:22','2018-05-07 20:04:07'),(199,1,10,'active','2018-05-07 20:04:44','2018-05-07 20:04:07','2018-05-07 20:04:44'),(200,1,13,'active','2018-05-07 20:06:07','2018-05-07 20:04:44','2018-05-07 20:06:07'),(201,1,14,'active','2018-05-07 20:06:07','2018-05-07 20:04:44','2018-05-07 20:06:07'),(202,1,15,'active','2018-05-07 20:06:07','2018-05-07 20:04:44','2018-05-07 20:06:07'),(203,1,15,'active','2018-05-07 20:06:31','2018-05-07 20:06:07','2018-05-07 20:06:31'),(204,1,14,'active','2018-05-07 20:10:05','2018-05-07 20:06:31','2018-05-07 20:10:05'),(205,1,15,'active','2018-05-07 20:10:05','2018-05-07 20:06:31','2018-05-07 20:10:05'),(206,1,14,'active','2018-05-07 20:10:34','2018-05-07 20:10:05','2018-05-07 20:10:34'),(207,1,15,'active','2018-05-07 20:10:34','2018-05-07 20:10:05','2018-05-07 20:10:34'),(208,1,14,'active','2018-05-07 20:10:39','2018-05-07 20:10:34','2018-05-07 20:10:39'),(209,1,15,'active','2018-05-07 20:10:39','2018-05-07 20:10:34','2018-05-07 20:10:39'),(210,1,10,'active','2018-05-07 21:20:03','2018-05-07 20:10:39','2018-05-07 21:20:03'),(211,1,13,'active',NULL,'2018-05-07 20:10:39','2018-05-07 20:10:39'),(212,1,14,'active','2018-05-07 21:20:15','2018-05-07 20:10:39','2018-05-07 21:20:15'),(213,1,15,'active',NULL,'2018-05-07 20:10:39','2018-05-07 20:10:39'),(214,53,13,'active',NULL,'2018-05-11 01:30:18','2018-05-11 01:30:18'),(215,53,15,'active',NULL,'2018-05-11 01:30:18','2018-05-11 01:30:18');

/*Table structure for table `userseeds_detail` */

DROP TABLE IF EXISTS `userseeds_detail`;

CREATE TABLE `userseeds_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `seed_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seed_id` int(11) NOT NULL,
  `supplier_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `density` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_userseed_id` int(11) NOT NULL,
  `measurement` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tray_size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `soak_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `germination` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `situation` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medium` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `maturity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `yield` int(11) DEFAULT NULL,
  `seeds_measurement` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'active',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `userseeds_detail` */

insert  into `userseeds_detail`(`id`,`seed_name`,`seed_id`,`supplier_name`,`density`,`user_userseed_id`,`measurement`,`tray_size`,`soak_status`,`germination`,`situation`,`medium`,`maturity`,`yield`,`seeds_measurement`,`notes`,`status`,`deleted_at`,`created_at`,`updated_at`) values (13,'ARUGULA',1,'1','.78',40,'ML','5 X 5','1','2','IN DARKNESS','MAT','18',7,'OUNCES','tets','active','2018-05-01 05:12:42','2018-04-30 12:45:53','2018-05-01 05:12:42'),(19,'ARUGULA',1,'1','.78',40,'ML','5 X 5','1','2','IN DARKNESS','MAT','18',7,'OUNCES','tets','active','2018-05-01 05:12:42','2018-04-30 13:00:01','2018-05-01 05:12:42'),(20,'ARUGULA',1,'1','.78',47,'OUNCES','5 X 5','2','2','IN DARKNESS','MAT','18',9,'OUNCES','tets','active','2018-05-01 05:12:42','2018-05-01 02:22:41','2018-05-01 05:12:42'),(22,'Chetan Seed',9,'1','Chetan Density',48,'OUNCES','5 X 5','2','16','COVER WITH SOIL (SOIL)','MAT','15',15,'GRAMS','Notes growth','active','2018-05-01 06:31:18','2018-05-01 06:16:01','2018-05-01 06:31:18'),(24,'A seeds',12,'1','Test seed density',48,'GRAMS','5 X 5','2','17','PLANT ON TOP (SOIL)','SOIL','5',1200,'GRAMS','Npotes','active','2018-05-01 06:57:41','2018-05-01 06:57:30','2018-05-01 06:57:41'),(25,'Hasmat Seed',10,'3','Hasmat density',48,'ML','5 X 5','1','14','COVER WITH SOIL (SOIL)','SOIL','18',1200,'OUNCES','Test notes','active','2018-05-01 07:00:11','2018-05-01 06:58:31','2018-05-01 07:00:11'),(26,'Hasmat Seed',10,'3','Hasmat density',48,'OUNCES','10 X 20','1','17','COVER WITH SOIL (SOIL)','SOIL','20',1200,'GRAMS','Test notes','active','2018-05-01 07:16:10','2018-05-01 07:01:01','2018-05-01 07:16:10'),(27,'Hasmat Seed',10,'3','Hasmat density',48,'OUNCES','5 X 5','2','14','COVER WITH SOIL (SOIL)','MAT','12',1200,'OUNCES','Test notes','active','2018-05-01 07:16:10','2018-05-01 07:02:57','2018-05-01 07:16:10'),(28,'A seeds',12,'1','Test seed density',48,'GRAMS','5 X 5','2','17','IN LIGHT','SOIL','5',1200,'GRAMS','Npotesgyv','active','2018-05-01 07:06:24','2018-05-01 07:05:26','2018-05-01 07:06:24'),(29,'Hasmat Seed',10,'10','Hasmat density',46,'GRAMS','5 X 5','2','14','IN LIGHT','MAT','12',1200,'OUNCES','Test notes','active','2018-05-01 07:16:10','2018-05-01 07:10:54','2018-05-01 07:16:10'),(30,'Test seeds',13,'10','Seed Density',48,'ML','18 X 26','1','19','PLANT ON TOP (SOIL)','MAT','16',1000,'OUNCES','test growth notes','active','2018-05-01 07:16:08','2018-05-01 07:14:31','2018-05-01 07:16:08'),(31,'My personal seeds',14,'10','as',48,'ML','18 X 26','1','20','COVER WITH SOIL (SOIL)','MAT','20',1000,'OUNCES','test notest','active','2018-05-01 07:16:06','2018-05-01 07:15:04','2018-05-01 07:16:06'),(32,'Hasmat Seed',10,'10','Hasmat density',48,'GRAMS','5 X 5','2','20','COVER WITH SOIL (SOIL)','MAT','20',1200,'ML','Test notes','active','2018-05-07 21:20:03','2018-05-01 07:17:58','2018-05-07 21:20:03'),(33,'Hasmat Seed',10,'10','Hasmat density',39,'ML','18 X 26','2','14','IN LIGHT','MAT','12',1200,'OUNCES','Test notes','active','2018-05-07 21:20:03','2018-05-01 12:15:39','2018-05-07 21:20:03'),(34,'My personal seeds',14,'10','as',39,'GRAMS','18 X 26','2','27','PLANT ON TOP (SOIL)','MAT','20',1000,'OUNCES','test notest growth','active','2018-05-07 21:20:15','2018-05-01 12:16:05','2018-05-07 21:20:15'),(35,'Test seeds',13,'10','Seed Density',39,'GRAMS','10 X 20','1','19','PLANT ON TOP (SOIL)','SOIL','30',1000,'OUNCES','test growth notes','active',NULL,'2018-05-01 12:16:28','2018-05-01 12:16:28'),(36,'My personal seeds',14,'10','as',1,'OUNCES','10 X 20','1','20','IN LIGHT','MAT','20',1000,'OUNCES','test notest','active','2018-05-07 21:20:15','2018-05-07 20:11:05','2018-05-07 21:20:15'),(37,'Test seeds',13,'10','Seed Density',1,'GRAMS','18 X 26','1','19','PLANT ON TOP (SOIL)','MAT','16',1000,'OUNCES','test growth notes','active',NULL,'2018-05-08 12:23:19','2018-05-08 12:23:19');

/*Table structure for table `variety` */

DROP TABLE IF EXISTS `variety`;

CREATE TABLE `variety` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'active',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=182 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `variety` */

insert  into `variety`(`id`,`name`,`status`,`deleted_at`,`created_at`,`updated_at`) values (1,'ORGANIC','active',NULL,NULL,NULL),(2,'RED GARNET (ORGANIC)','active',NULL,NULL,NULL),(3,'RED GARNET','active',NULL,NULL,NULL),(4,'SLOW BOLT','active',NULL,NULL,NULL),(5,'CINNAMON','active',NULL,NULL,NULL),(6,'DARK OPAL','active',NULL,NULL,NULL),(7,'GENOVESE','active',NULL,NULL,NULL),(8,'THAI','active',NULL,NULL,NULL),(9,'BULL\'S BLOOD','active',NULL,NULL,NULL),(10,'DETROIT DARK RED','active',NULL,NULL,NULL),(11,'PURPLE SPROUTING (ORGANIC)','active',NULL,NULL,NULL),(12,'WALTHAM 29','active',NULL,NULL,NULL),(13,'LONG ISLAND IMPROVED','active',NULL,NULL,NULL),(14,'GOLDEN ACRE','active',NULL,NULL,NULL),(15,'MAMMOTH RED ROCK','active',NULL,NULL,NULL),(16,'PAK CHOI WHITE STEM','active',NULL,NULL,NULL),(17,'RED ACRE','active',NULL,NULL,NULL),(18,'SNOWBALL Y IMPROVED','active',NULL,NULL,NULL),(19,'UTAH 52-70','active',NULL,NULL,NULL),(20,'CURLED','active',NULL,NULL,NULL),(21,'SLOW BOLT','active',NULL,NULL,NULL),(22,'RED (ORGANIC)','active',NULL,NULL,NULL),(23,'VATES','active',NULL,NULL,NULL),(24,'YELLOW POPCORN(ORGANIC)','active',NULL,NULL,NULL),(25,'UPLAND','active',NULL,NULL,NULL),(26,'BOUQUET','active',NULL,NULL,NULL),(27,'BROADLEAF BATAVIAN','active',NULL,NULL,NULL),(28,'GREEN CURLED RUFFEC','active',NULL,NULL,NULL),(29,'BRONZE','active',NULL,NULL,NULL),(30,'FLORENCE','active',NULL,NULL,NULL),(31,'DWARF SIBERIAN','active',NULL,NULL,NULL),(32,'LACINATO(ORGANIC)','active',NULL,NULL,NULL),(33,'LACINATO','active',NULL,NULL,NULL),(34,'RED RUSSIAN','active',NULL,NULL,NULL),(35,'VATES BLUE SCOTCH CURLED','active',NULL,NULL,NULL),(36,'EARLY WHITE VIENNA','active',NULL,NULL,NULL),(37,'PURPLE VIENNA','active',NULL,NULL,NULL),(38,'LARGE AMERICAN FLAG','active',NULL,NULL,NULL),(39,'CRISPHEAD GREAT LAKES 118','active',NULL,NULL,NULL),(40,'GOURMET MIXTURE','active',NULL,NULL,NULL),(41,'PARRIS ISLAND COS','active',NULL,NULL,NULL),(42,'WALDMANN\'S GREEN','active',NULL,NULL,NULL),(43,'HANSON IMPROVED','active',NULL,NULL,NULL),(44,'ICEBERG','active',NULL,NULL,NULL),(45,'GOURMET MIX (ORGANIC)','active',NULL,NULL,NULL),(46,'GRAND RAPIDS','active',NULL,NULL,NULL),(47,'LOLLO ROSSO (ORGANIC)','active',NULL,NULL,NULL),(48,'PRIZEHEAD','active',NULL,NULL,NULL),(49,'RED SAILS','active',NULL,NULL,NULL),(50,'RUBY RED','active',NULL,NULL,NULL),(51,'SALAD BOWL GREEN','active',NULL,NULL,NULL),(52,'SALAD BOWL RED','active',NULL,NULL,NULL),(53,'MESCLUN MIX','active',NULL,NULL,NULL),(54,'CIMMARON','active',NULL,NULL,NULL),(55,'FRECKLES','active',NULL,NULL,NULL),(56,'LITTLE GEM','active',NULL,NULL,NULL),(57,'GEM SERIES','active',NULL,NULL,NULL),(58,'MIZUNA RED LEAF','active',NULL,NULL,NULL),(59,'MIZUNA RED STREAKS (ORGANIC)','active',NULL,NULL,NULL),(60,'MIZUNA RED STREAKS','active',NULL,NULL,NULL),(61,'OSAKA PURPLE','active',NULL,NULL,NULL),(62,'RED GARNET','active',NULL,NULL,NULL),(63,'RED GIANT','active',NULL,NULL,NULL),(64,'SOUTHERN GIANT CURLED','active',NULL,NULL,NULL),(65,'SPICY ORIENTAL (ORGANIC)','active',NULL,NULL,NULL),(66,'SPICY ORIENTAL','active',NULL,NULL,NULL),(67,'SPINACH (KOMATSUNA)','active',NULL,NULL,NULL),(68,'TATSOI','active',NULL,NULL,NULL),(69,'TOKYO BEKANA','active',NULL,NULL,NULL),(70,'WASABI (ORGANIC)','active',NULL,NULL,NULL),(71,'WASABI','active',NULL,NULL,NULL),(72,'JEWEL MIX','active',NULL,NULL,NULL),(73,'BUNCHING EVERGREEN (ORGANIC)','active',NULL,NULL,NULL),(74,'BUNCHING TOKYO LONG WHITE','active',NULL,NULL,NULL),(75,'CRYSTAL WHITE WAX','active',NULL,NULL,NULL),(76,'RED BURGUNDY','active',NULL,NULL,NULL),(77,'SOUTHPORT RED GLOBE','active',NULL,NULL,NULL),(78,'UTAH YELLOW SWEET SPANISH','active',NULL,NULL,NULL),(79,'WHITE SWEET SPANISH','active',NULL,NULL,NULL),(80,'PURPLE','active',NULL,NULL,NULL),(81,'COMMON ITALIAN (ORGANIC)','active',NULL,NULL,NULL),(82,'GREEK','active',NULL,NULL,NULL),(83,'DARK GREEN ITALIAN FLAT-LEAF','active',NULL,NULL,NULL),(84,'TRIPLE MOSS CURLED','active',NULL,NULL,NULL),(85,'DUN','active',NULL,NULL,NULL),(86,'EARLY FROSTY','active',NULL,NULL,NULL),(87,'GREEN (ORGANIC)','active',NULL,NULL,NULL),(88,'GREEN ARROW','active',NULL,NULL,NULL),(89,'LINCOLN','active',NULL,NULL,NULL),(90,'LITTLE MARVEL','active',NULL,NULL,NULL),(91,'SNOW DWARF SUGAR GREY','active',NULL,NULL,NULL),(92,'SNOW MAMMOTH MELTING SUGAR POD','active',NULL,NULL,NULL),(93,'SPECKLED (ORGANIC)','active',NULL,NULL,NULL),(94,'THOMAS LAXTON','active',NULL,NULL,NULL),(95,'CHAMPION','active',NULL,NULL,NULL),(96,'CHINA ROSE','active',NULL,NULL,NULL),(97,'DAIKON (ORGANIC)','active',NULL,NULL,NULL),(98,'HONG VIT (ORGANIC)','active',NULL,NULL,NULL),(99,'MINOWASE','active',NULL,NULL,NULL),(100,'RAMBO (ORGANIC)','active',NULL,NULL,NULL),(101,'RED ARROW','active',NULL,NULL,NULL),(102,'SANGO PURPLE (ORGANIC)','active',NULL,NULL,NULL),(103,'SANGO PURPLE','active',NULL,NULL,NULL),(104,'TRITON PURPLE','active',NULL,NULL,NULL),(105,'AMERICAN PURPLE TOP','active',NULL,NULL,NULL),(106,'LARGE LEAF','active',NULL,NULL,NULL),(107,'RED VEINED','active',NULL,NULL,NULL),(108,'BLACK OIL (ORGANIC)','active',NULL,NULL,NULL),(109,'RAINBOW MIXTURE','active',NULL,NULL,NULL),(110,'RUBY RED','active',NULL,NULL,NULL),(111,'PURPLE TOP WHITE GLOBE','active',NULL,NULL,NULL),(112,'WASABI','active',NULL,NULL,NULL),(113,'BICOLOR ORGANIC','active',NULL,NULL,NULL),(114,'CINNAMON','active',NULL,NULL,NULL),(115,'DARK OPAL','active',NULL,NULL,NULL),(116,'GENOVESE','active',NULL,NULL,NULL),(117,'ITALIAN LARGE LEAF ORGANIC','active',NULL,NULL,NULL),(118,'ITALIAN LARGE LEAF','active',NULL,NULL,NULL),(119,'LEMON','active',NULL,NULL,NULL),(120,'RED RUBIN','active',NULL,NULL,NULL),(121,'RED RUBIN ORGANIC','active',NULL,NULL,NULL),(122,'BULL\'S BLOOD ORGANIC','active',NULL,NULL,NULL),(123,'BULL\'S BLOOD','active',NULL,NULL,NULL),(124,'EARLY WONDER TALL TOP','active',NULL,NULL,NULL),(125,'YELLOW','active',NULL,NULL,NULL),(126,'RED ORGANIC','active',NULL,NULL,NULL),(127,'RED','active',NULL,NULL,NULL),(128,'BRIGHT LIGHTS','active',NULL,NULL,NULL),(129,'RUBY RED','active',NULL,NULL,NULL),(130,'RUBY RED ORGANIC','active',NULL,NULL,NULL),(131,'BIANCA BIANCA','active',NULL,NULL,NULL),(132,'KOGANE','active',NULL,NULL,NULL),(133,'TOKYO BEKANA','active',NULL,NULL,NULL),(134,'CHAMPION','active',NULL,NULL,NULL),(135,'VATES','active',NULL,NULL,NULL),(136,'CRESSIDA ORGANIC','active',NULL,NULL,NULL),(137,'CRESSIDA','active',NULL,NULL,NULL),(138,'PERSIAN ORGANIC','active',NULL,NULL,NULL),(139,'BRONZE ORGANIC','active',NULL,NULL,NULL),(140,'GREEN','active',NULL,NULL,NULL),(141,'GREEN ORGANIC','active',NULL,NULL,NULL),(142,'RED RUSSIAN ORGANIC','active',NULL,NULL,NULL),(143,'RED RUSSIAN','active',NULL,NULL,NULL),(144,'TOSCANO ORGANIC','active',NULL,NULL,NULL),(145,'TOSCANO','active',NULL,NULL,NULL),(146,'PURPLE ORGANIC','active',NULL,NULL,NULL),(147,'PURPLE','active',NULL,NULL,NULL),(148,'GEM','active',NULL,NULL,NULL),(149,'MIZ AMERICA (F1)','active',NULL,NULL,NULL),(150,'RED KINGDOM (F1)','active',NULL,NULL,NULL),(151,'BARBAROSSA','active',NULL,NULL,NULL),(152,'GARNET GIANT','active',NULL,NULL,NULL),(153,'GOLDEN FRILLS','active',NULL,NULL,NULL),(154,'GREEN WAVE ORGANIC','active',NULL,NULL,NULL),(155,'GREEN WAVE','active',NULL,NULL,NULL),(156,'RED GIANT','active',NULL,NULL,NULL),(157,'RED GIANT ORGANIC','active',NULL,NULL,NULL),(158,'RUBY STREAKS','active',NULL,NULL,NULL),(159,'SCARLET FRILLS','active',NULL,NULL,NULL),(160,'SUEHLIHUNG NO. 2','active',NULL,NULL,NULL),(161,'WASABINA','active',NULL,NULL,NULL),(162,'RUBY RED','active',NULL,NULL,NULL),(163,'SCARLET','active',NULL,NULL,NULL),(164,'RED CHOI (F1)','active',NULL,NULL,NULL),(165,'RED PAC (F1)','active',NULL,NULL,NULL),(166,'ROSIE (F1)','active',NULL,NULL,NULL),(167,'RED GRUNER','active',NULL,NULL,NULL),(168,'DAIKON ORGANIC','active',NULL,NULL,NULL),(169,'HONG VIT','active',NULL,NULL,NULL),(170,'RED ARROW','active',NULL,NULL,NULL),(171,'RED RAMBO','active',NULL,NULL,NULL),(172,'RED RAMBO ORGANIC','active',NULL,NULL,NULL),(173,'EVERGREEN HARDY WHITE','active',NULL,NULL,NULL),(174,'BRITTON','active',NULL,NULL,NULL),(175,'RED VEINED','active',NULL,NULL,NULL),(176,'','active','2018-05-01 07:19:48',NULL,'2018-05-01 07:19:48'),(177,'test','inactive','2018-05-01 12:13:01','2018-05-01 07:18:24','2018-05-01 12:13:01'),(178,'My VARIETYS','inactive','2018-05-01 07:23:27','2018-05-01 07:21:31','2018-05-01 07:23:27'),(179,'TESTING VARIETYS','active','2018-05-01 07:31:22','2018-05-01 07:24:47','2018-05-01 07:31:22'),(180,'Test variety that has been edited','active',NULL,'2018-05-01 07:32:30','2018-05-14 18:47:14'),(181,'My Personal Variety','active',NULL,'2018-05-01 12:12:00','2018-05-14 18:46:20');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
